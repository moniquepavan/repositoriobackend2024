package org.example;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;

public class JogadorGerador {
    private static String listaDeNomes;
    private static String listaDeSobrenomes;
    private static String listaDePosicoes;
    private static String listaDeClubes;

    static {
        try {
            listaDeNomes = retornaListaDaURI("https://venson.net.br/resources/data/nomes.txt");
            listaDeSobrenomes = retornaListaDaURI("https://venson.net.br/resources/data/sobrenomes.txt");
            listaDePosicoes = retornaListaDaURI("https://venson.net.br/resources/data/posicoes.txt");
            listaDeClubes = retornaListaDaURI("https://venson.net.br/resources/data/clubes.txt");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Jogador geraJogador() {
        String nome = retornaIndiceAleatorio(listaDeNomes);
        String sobrenome = retornaIndiceAleatorio(listaDeSobrenomes);
        String posicao = retornaIndiceAleatorio(listaDePosicoes);
        String clube = retornaIndiceAleatorio(listaDeClubes);
        int idade = geraNumeroAleatorio(17, 40);
        return new Jogador(nome, sobrenome, posicao, idade, clube);
    }

    private static String retornaListaDaURI(String minhaUri) throws Exception {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(minhaUri)).build();
        HttpResponse<String> response = client.send(request, BodyHandlers.ofString());
        return response.body();
    }

    private static String retornaIndiceAleatorio(String stringao) {
        String[] lista = stringao.split("\n");
        int indiceAleatorio = (int) Math.floor(Math.random() * lista.length);
        return lista[indiceAleatorio];
    }

    private static int geraNumeroAleatorio(int menor, int maior) {
        int diferenca = maior - menor + 1;
        return (int) Math.floor(Math.random() * diferenca) + menor;
    }
}
